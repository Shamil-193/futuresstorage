export interface Futures {
  date: Date,
  value: number,
}


export interface MinMaxValues {
  min: Futures,
  max: Futures,
}

export interface MinMaxFuturesValues {
  oneDay: MinMaxValues,
  fourHours: MinMaxValues,
  oneHour: MinMaxValues,
  fifteenMin: MinMaxValues,
  fiveMin: MinMaxValues,
}


export class FuturesStorage {

  private store: Map<Date, number> = new Map();
  private sortedStore: Map<Date, number>  = new Map();
  private minMaxValuesState: MinMaxFuturesValues | null = null;
  private isStoreSorted: boolean = false;

  private intervalName: string[] = ['oneDay', 'fourHours', 'oneHour', 'fifteenMin', 'fiveMin',]
  private intervalLength: {[key: string]: number} = {
    oneDay: 24*60*60*1000,
    fourHours: 4*60*60*1000,
    oneHour: 60*60*1000,
    fifteenMin: 15*60*1000,
    fiveMin: 5*60*1000,
  }

  public setFutures(futures: Futures): void {

    // сначала добавляем новое значение в стор и удаляем из стора данные старше 24 часов.
    // 17280 - это количество записей за сутки при периоде обновления 5 сек
    this.store.set(futures.date, futures.value);
    if(this.store.size > 17280){
      // нахожу первый элемент и удаляю его
      const [[firstKey]] = this.store.entries();
      this.store.delete(firstKey);
    }

    // проверяем было ли первое присвоение. Если нет, то присваиваю
    if(this.minMaxValuesState === null) {
      this.minMaxValuesState = {
        oneDay: { min: {...futures}, max: {...futures} },
        fourHours: { min: {...futures}, max: {...futures} },
        oneHour: { min: {...futures}, max: {...futures} },
        fifteenMin: { min: {...futures}, max: {...futures} },
        fiveMin: { min: {...futures}, max: {...futures} },
      };
    } else {
      // стор уже не пустой, значит отрабатываем алгоритм проверки мин и макс значений
      const currentDate: number = +new Date()

      // пробегаю по всем временным интервалам и выполняю алгоритм проверки и обновления
      for(let intervalName of this.intervalName){
          this.checkAndUpdateIntervalValues({
            currentDate,
            intervalName,
            futures,
          })
      }

    }
    // обнуляем триггер сортировки стора
    this.isStoreSorted = false;
  }

  public getMaxFuturesValueInTerm(): MinMaxFuturesValues | null {
    return this.minMaxValuesState
  }

  private updateMax({
                      futures,
                      currentIntervalValues,
                      interval,
                      currentDate
  } : {futures: Futures, currentIntervalValues: MinMaxValues, interval: number, currentDate: number,}
  ): void {

    // если пришедшее значение не меньше текущего и оно укладывает в интервал времени, то обновляем значение в minMaxValuesState
    if (currentIntervalValues.max.value <= futures.value && currentDate - +futures.date <= interval) {
      currentIntervalValues.max = {...futures};
      console.log('1 обновил МАКС значение на', futures.value);
    } else {
      // Иначе проверяем не устарело ли текущее значение для minMaxValuesState.oneDay.max
      if(currentDate - +currentIntervalValues.max.date >= interval){
        // если устарело, значит обновляем значение
        // сортирую по возрастанию стор, если он не еще не сортирован
        if(!this.isStoreSorted){
          this.sortedStore = new Map([...this.store.entries()].sort((a, b) => b[1] - a[1]));
          // ставим триггер, чтобы лишний раз не сортировать стор на одной иттерации
          this.isStoreSorted = true;
        }

        // перебираем отсортированный стор и находим первый подходящий по интервалу фьючерс
        for (const [key, value] of this.sortedStore.entries()) {
          if (currentDate - +key <= interval) {
            currentIntervalValues.max.value = value;
            currentIntervalValues.max.date = key;
            console.log('2 обновил МАКС значение на', value);
            break;
          }
        }
      }
      // если не устарело значение - ничего не делаем
    }
  }

  private updateMin({
                      futures,
                      currentIntervalValues,
                      interval,
                      currentDate
                    }: {futures: Futures, currentIntervalValues: MinMaxValues, interval: number, currentDate: number}): void {
    // Если пришедшее значение больше текущего и оно укладывается в интервал времени, обновляем значение в minMaxValuesState
    if (currentIntervalValues.min.value >= futures.value && currentDate - +futures.date <= interval) {
      currentIntervalValues.min = {...futures};
      console.log('1 обновил МИН значение на ', futures.value);
    } else {
      // Иначе проверяем, не устарело ли текущее значение для minMaxValuesState.min
      if(currentDate - +currentIntervalValues.min.date >= interval){
        // Если устарело, обновляем значение
        // Сортируем по возрастанию стор, если он еще не отсортирован
        if(!this.isStoreSorted){
          this.sortedStore = new Map([...this.store.entries()].sort((a, b) => b[1] - a[1]));
          // Ставим триггер, чтобы лишний раз не сортировать стор на одной итерации
          this.isStoreSorted = true;
        }
        // сделаем порядок по возрастанию
        const reversedSortedStore = new Map([...this.sortedStore.entries()].reverse());
        // Перебираем отсортированный стор и находим первый подходящий по интервалу фьючерс
        for (const [key, value] of reversedSortedStore.entries()) {
          if (currentDate - +key <= interval) {
            currentIntervalValues.min.value = value;
            currentIntervalValues.min.date = key;
            console.log('2 обновил МИН значение на', value);
            break;
          }
        }
      }
      // Если значение не устарело, ничего не делаем
    }
  }

  private checkAndUpdateIntervalValues(
    {currentDate, intervalName, futures}: {currentDate: number, intervalName: string, futures: Futures }
  ): void {
    // создаю константы
    const interval: number = this.intervalLength[intervalName];
    const currentIntervalIndex: number = this.intervalName.indexOf(intervalName);
    const currentIntervalValues: MinMaxValues = this.minMaxValuesState[intervalName as keyof MinMaxFuturesValues]

    // если текущий интервал oneDay, то иду функцию updateMax
    if(currentIntervalIndex === 0) {
      this.updateMax({
        futures,
        currentIntervalValues,
        interval,
        currentDate
      });
      this.updateMin({
        futures,
        currentIntervalValues,
        interval,
        currentDate
      });
    } else {
      // Иначе проверяю, подходит ли максимальное значение для более длительного интервала. Такая проверка ускоряет алгоритм
      const previousIntervalName = this.intervalName[currentIntervalIndex - 1] as keyof MinMaxFuturesValues;
      const previousIntervalValues: MinMaxValues = this.minMaxValuesState[previousIntervalName];

      // сама проверка
      if (currentDate - +previousIntervalValues.max.date <= interval && previousIntervalValues.max.date != currentIntervalValues.max.date) {
        // если подходит, то присваиваем значение
        currentIntervalValues.max = { ...previousIntervalValues.max };
        console.log('3 обновил МАКС значение на', previousIntervalValues.max.value);
      } else {
        // есди не подходит, то бежим в функцию updateMax
        this.updateMax({
          futures,
          currentIntervalValues,
          interval,
          currentDate
        });
      }

      // Такой же процесс для минимального значение для более длительного интервала.
      if (currentDate - +previousIntervalValues.min.date <= interval && previousIntervalValues.min.date != currentIntervalValues.min.date) {
        // есди подходит, то присваиваем значение
        currentIntervalValues.min = { ...previousIntervalValues.min };
        console.log('3 обновил МИН значение на', previousIntervalValues.min.value);
      } else {
        // есди не подходит, то бежим в функцию updateMin
        this.updateMin({
          futures,
          currentIntervalValues,
          interval,
          currentDate
        });
      }
    }

  }
}


let Futures1 = new FuturesStorage();
setInterval(() => {
  const features: Futures = {date: new Date(), value: getRandomInt(0, 100)}
  console.log('sent futures is ', features);
  Futures1.setFutures(features);
  console.log('get back values are ', Futures1.getMaxFuturesValueInTerm());
}, 5000);


function getRandomInt(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}



