"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FuturesStorage = void 0;
class FuturesStorage {
    constructor() {
        this.store = new Map();
        this.sortedStore = new Map();
        this.minMaxValuesState = null;
        this.isStoreSorted = false;
        this.intervalName = ['oneDay', 'fourHours', 'oneHour', 'fifteenMin', 'fiveMin',];
        this.intervalLength = {
            oneDay: 24 * 60 * 60 * 1000,
            fourHours: 4 * 60 * 60 * 1000,
            oneHour: 60 * 60 * 1000,
            fifteenMin: 15 * 60 * 1000,
            fiveMin: 5 * 60 * 1000,
        };
    }
    setFutures(futures) {
        // сначала добавляем новое значение в стор и удаляем из стора данные старше 24 часов.
        // 17280 - это количество записей за сутки при периоде обновления 5 сек
        this.store.set(futures.date, futures.value);
        if (this.store.size > 17280) {
            // нахожу первый элемент и удаляю его
            const [[firstKey]] = this.store.entries();
            this.store.delete(firstKey);
        }
        // проверяем было ли первое присвоение. Если нет, то присваиваю
        if (this.minMaxValuesState === null) {
            this.minMaxValuesState = {
                oneDay: { min: Object.assign({}, futures), max: Object.assign({}, futures) },
                fourHours: { min: Object.assign({}, futures), max: Object.assign({}, futures) },
                oneHour: { min: Object.assign({}, futures), max: Object.assign({}, futures) },
                fifteenMin: { min: Object.assign({}, futures), max: Object.assign({}, futures) },
                fiveMin: { min: Object.assign({}, futures), max: Object.assign({}, futures) },
            };
        }
        else {
            // стор уже не пустой, значит отрабатываем алгоритм проверки мин и макс значений
            const currentDate = +new Date();
            // пробегаю по всем временным интервалам и выполняю алгоритм проверки и обновления
            for (let intervalName of this.intervalName) {
                this.checkAndUpdateIntervalValues({
                    currentDate,
                    intervalName,
                    futures,
                });
            }
        }
        // обнуляем триггер сортировки стора
        this.isStoreSorted = false;
    }
    getMaxFuturesValueInTerm() {
        return this.minMaxValuesState;
    }
    updateMax({ futures, currentIntervalValues, interval, currentDate }) {
        // если пришедшее значение не меньше текущего и оно укладывает в интервал времени, то обновляем значение в minMaxValuesState
        if (currentIntervalValues.max.value <= futures.value && currentDate - +futures.date <= interval) {
            currentIntervalValues.max = Object.assign({}, futures);
            console.log('1 обновил МАКС значение на', futures.value);
        }
        else {
            // Иначе проверяем не устарело ли текущее значение для minMaxValuesState.oneDay.max
            if (currentDate - +currentIntervalValues.max.date >= interval) {
                // если устарело, значит обновляем значение
                // сортирую по возрастанию стор, если он не еще не сортирован
                if (!this.isStoreSorted) {
                    this.sortedStore = new Map([...this.store.entries()].sort((a, b) => b[1] - a[1]));
                    // ставим триггер, чтобы лишний раз не сортировать стор на одной иттерации
                    this.isStoreSorted = true;
                }
                // перебираем отсортированный стор и находим первый подходящий по интервалу фьючерс
                for (const [key, value] of this.sortedStore.entries()) {
                    if (currentDate - +key <= interval) {
                        currentIntervalValues.max.value = value;
                        currentIntervalValues.max.date = key;
                        console.log('2 обновил МАКС значение на', value);
                        break;
                    }
                }
            }
            // если не устарело значение - ничего не делаем
        }
    }
    updateMin({ futures, currentIntervalValues, interval, currentDate }) {
        // Если пришедшее значение больше текущего и оно укладывается в интервал времени, обновляем значение в minMaxValuesState
        if (currentIntervalValues.min.value >= futures.value && currentDate - +futures.date <= interval) {
            currentIntervalValues.min = Object.assign({}, futures);
            console.log('1 обновил МИН значение на ', futures.value);
        }
        else {
            // Иначе проверяем, не устарело ли текущее значение для minMaxValuesState.min
            if (currentDate - +currentIntervalValues.min.date >= interval) {
                // Если устарело, обновляем значение
                // Сортируем по возрастанию стор, если он еще не отсортирован
                if (!this.isStoreSorted) {
                    this.sortedStore = new Map([...this.store.entries()].sort((a, b) => b[1] - a[1]));
                    // Ставим триггер, чтобы лишний раз не сортировать стор на одной итерации
                    this.isStoreSorted = true;
                }
                // сделаем порядок по возрастанию
                const reversedSortedStore = new Map([...this.sortedStore.entries()].reverse());
                // Перебираем отсортированный стор и находим первый подходящий по интервалу фьючерс
                for (const [key, value] of reversedSortedStore.entries()) {
                    if (currentDate - +key <= interval) {
                        currentIntervalValues.min.value = value;
                        currentIntervalValues.min.date = key;
                        console.log('2 обновил МИН значение на', value);
                        break;
                    }
                }
            }
            // Если значение не устарело, ничего не делаем
        }
    }
    checkAndUpdateIntervalValues({ currentDate, intervalName, futures }) {
        // создаю константы
        const interval = this.intervalLength[intervalName];
        const currentIntervalIndex = this.intervalName.indexOf(intervalName);
        const currentIntervalValues = this.minMaxValuesState[intervalName];
        // если текущий интервал oneDay, то иду функцию updateMax
        if (currentIntervalIndex === 0) {
            this.updateMax({
                futures,
                currentIntervalValues,
                interval,
                currentDate
            });
            this.updateMin({
                futures,
                currentIntervalValues,
                interval,
                currentDate
            });
        }
        else {
            // Иначе проверяю, подходит ли максимальное значение для более длительного интервала. Такая проверка ускоряет алгоритм
            const previousIntervalName = this.intervalName[currentIntervalIndex - 1];
            const previousIntervalValues = this.minMaxValuesState[previousIntervalName];
            // сама проверка
            if (currentDate - +previousIntervalValues.max.date <= interval && previousIntervalValues.max.date != currentIntervalValues.max.date) {
                // если подходит, то присваиваем значение
                currentIntervalValues.max = Object.assign({}, previousIntervalValues.max);
                console.log('3 обновил МАКС значение на', previousIntervalValues.max.value);
            }
            else {
                // есди не подходит, то бежим в функцию updateMax
                this.updateMax({
                    futures,
                    currentIntervalValues,
                    interval,
                    currentDate
                });
            }
            // Такой же процесс для минимального значение для более длительного интервала.
            if (currentDate - +previousIntervalValues.min.date <= interval && previousIntervalValues.min.date != currentIntervalValues.min.date) {
                // есди подходит, то присваиваем значение
                currentIntervalValues.min = Object.assign({}, previousIntervalValues.min);
                console.log('3 обновил МИН значение на', previousIntervalValues.min.value);
            }
            else {
                // есди не подходит, то бежим в функцию updateMin
                this.updateMin({
                    futures,
                    currentIntervalValues,
                    interval,
                    currentDate
                });
            }
        }
    }
}
exports.FuturesStorage = FuturesStorage;
let Futures1 = new FuturesStorage();
setInterval(() => {
    const features = { date: new Date(), value: getRandomInt(0, 100) };
    console.log('sent futures is ', features);
    Futures1.setFutures(features);
    console.log('get back values are ', Futures1.getMaxFuturesValueInTerm());
}, 5000);
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
